﻿using NewDay.Digital.Foundation.Connector.FirstData.Brands;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface IBrandService
    {
        Brand GetBrandFromAccountNumber(string account);
    }
}
