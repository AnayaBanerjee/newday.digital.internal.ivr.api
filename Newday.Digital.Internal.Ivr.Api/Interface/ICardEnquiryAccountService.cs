﻿using NewDay.Digital.Foundation.Connector.FirstData.AccountManagement.v2.Models;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface ICardEnquiryAccountService
    {
        Task<CardsListByAccountResponse2> cardenquiryaccount(CardsListByAccountRequest2 request);
    }
}
