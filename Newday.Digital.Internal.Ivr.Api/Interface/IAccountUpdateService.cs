﻿using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.AccountUpdate.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.Transactions.v1.Models;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface IAccountUpdateService
    {
        Task<DirectCreditUpdateResponse> DirectCreditUpdate(DirectCreditUpdateRequest directCreditUpdateRequest);
        Task<MemoAddResponse> MemoUpdate(MemoAddRequest request);
        Task<MonetaryActionResponse> MonetaryActionUpdate(MonetaryActionRequest request);
    }
}
