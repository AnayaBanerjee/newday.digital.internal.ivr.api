﻿using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1.Models;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface IPinUnlockUpdateService
    {
        Task<CardUpdateResponse> PinUnlockUpdate(CardUpdateRequest request);
    }
}
