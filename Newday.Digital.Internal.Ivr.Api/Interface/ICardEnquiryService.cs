﻿using NewDay.Digital.Foundation.Connector.FirstData.CardManagement.v1.Models;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface ICardEnquiryService
    {
        Task<CardInquiryResponse> CardInquiry(CardInquiryRequest request);
    }
}
