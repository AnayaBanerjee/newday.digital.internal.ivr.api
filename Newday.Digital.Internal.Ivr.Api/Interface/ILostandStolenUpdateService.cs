﻿using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v2.Models;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface ILostandStolenUpdateService
    {
        Task<LostStolenUpdateResponse2> LostAndStolen(LostStolenUpdateRequest2 loststolenUpdateRequest);
        Task<MemoAddResponse> MemoUpdate(MemoAddRequest request);
    }
}
