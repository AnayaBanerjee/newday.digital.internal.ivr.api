﻿using Newday.Digital.Internal.Ivr.Api.Models.Atc;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Interface
{
    public interface IAtcService
    {
        Task<DeclineAtcResponse> DeclineOffer(AtcDeclineRequestDto atcDeclineRequestDto);
        Task<UpgradeOfferAtcResponse> UpgradeOffer(AtcUpgradeOfferRequestDto atcUpgradeOfferRequestDto);
    }
}
