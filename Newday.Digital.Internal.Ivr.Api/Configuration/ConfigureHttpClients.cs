﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newday.Digital.Internal.Ivr.Api.Models;
using System;

namespace Newday.Digital.Internal.Ivr.Api.Configuration
{
    public static class ConfigureHttpClients
    {
        public static IServiceCollection AddHttpClients(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHttpClient(HttpClients.AtcClient, client =>
            {
                client.BaseAddress = new Uri(configuration["AtcServer:BaseUrl"]);
                client.Timeout = TimeSpan.FromMilliseconds(Convert.ToDouble(configuration["ClientTimeOut"]));
            });
            return services;
        }
    }
}
