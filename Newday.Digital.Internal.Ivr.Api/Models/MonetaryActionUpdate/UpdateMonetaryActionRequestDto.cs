﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.MonetaryActionUpdate
{
    public class UpdateMonetaryActionRequestDto
    {
        //
        // Summary:
        //     Max length = 19, ACCT NBR
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("acctNbr")]
        public string AcctNbr { get; set; }
        //
        // Summary:
        //     Max length = 11, TXN AMOUNT
        [StringLength(11, ErrorMessage = "value should be within range")]
        [JsonProperty("txnAmount")]
        public string TxnAmount { get; set; }

        //
        // Summary:
        //     Max length = 5, PLAN NBR
        [StringLength(5, ErrorMessage = "value should be within range")]
        [JsonProperty("planNbr")]
        public string PlanNbr { get; set; }

        //
        // Summary:
        //     Max length = 2, PLAN SEQ
        /* [JsonProperty("planSeq")]
         public string PlanSeq { get; set; }*/

        //
        // Summary:
        //     Max length = 1, FOREIGN USE
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("foreignUse")]
        public string ForeignUse { get; set; }

        //
        // Summary:
        //     Max length = 4, ACTION CODE
        [StringLength(4, ErrorMessage = "value should be within range")]
        [JsonProperty("actionCode")]
        public string ActionCode { get; set; }

        //
        // Summary:
        //     Format: YYYYMMDD. EFF DATE
        [StringLength(8, ErrorMessage = "value should be within range")]
        [JsonProperty("effDate")]
        public string EffDate { get; set; }

        //
        // Summary:
        //     Max length = 9, STORE NBR
        [StringLength(9, ErrorMessage = "value should be within range")]
        [JsonProperty("storeNbr")]
        public string StoreNbr { get; set; }

        //
        // Summary:
        //     Max length = 60, LINE 1
        [StringLength(200, ErrorMessage = "value should be within range")]
        [JsonProperty("line1")]
        public string Line1 { get; set; }

    }
}
