﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.DirectCreditUpdate
{
    public class UpdateDirectCreditRequestDto
    {
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("account")]
        public string Account { get; set; }

        [StringLength(8, ErrorMessage = "value should be within range")]
        [JsonProperty("sortcode")]
        public string SortCode { get; set; }

        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("bankacctnum")]
        public string Bankacctnum { get; set; }

        [StringLength(11, ErrorMessage = "value should be within range")]
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [StringLength(8, ErrorMessage = "value should be within range")]
        [JsonProperty("plannumber")]
        public string Plannumber { get; set; }
    }
}
