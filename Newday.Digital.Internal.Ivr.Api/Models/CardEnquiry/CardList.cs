﻿using NewDay.Digital.Foundation.Connector.FirstData.AccountManagement.v2.Models;
using System.Collections.Generic;

namespace Newday.Digital.Internal.Ivr.Api.Models.CardEnquiry
{
    public class CardList
    {
        public List<CardTbl2ForCardsListByAccount2> Cards { get; set; }
    }
}
