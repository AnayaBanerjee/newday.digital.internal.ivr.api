﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.CardEnquiry
{
    public class CardEnquiryRequestDto
    {
        //
        // Summary:
        //     Max length = 19, ACCNT NBR
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("accntNbr")]
        public string AccntNbr { get; set; }

        [JsonProperty("cardnbr")]
        public string Cardnbr { get; set; }

    }
}
