﻿using Newtonsoft.Json;
using System;

namespace Newday.Digital.Internal.Ivr.Api.Models.CardEnquiry
{
    public class CardEnquiryResponseDto
    {

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("memoSuccess")]
        public bool MemoSuccess { get; set; }

        [JsonProperty("message")]
        public String Message { get; set; }

        [JsonProperty("fraudMonitorFlag")]
        public String FraudMonitorFlag { get; set; }

        [JsonProperty("cardNumber")]
        public String CardNumber { get; set; }

        [JsonProperty("embPinCommPref")]
        public String EmbPinCommPref { get; set; }

        [JsonProperty("freezeStatus")]
        public String FreezeStatus { get; set; }
    }

}
