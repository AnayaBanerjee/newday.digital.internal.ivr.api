﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.DeflectionStatus
{
    public class DeflectStatusResponseDto
    {

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("deflect_status")]
        public string Deflect_status { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("skill")]
        public string Skill { get; set; }

        [JsonProperty("auth_unauth")]
        public string Auth_unauth { get; set; }
    }

}
