﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.DeflectionStatus
{
    public class DeflectStatusRequestDto
    {

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("skill")]
        public string Skill { get; set; }

        [JsonProperty("dept")]
        public string Dept { get; set; }

        [JsonProperty("authunauthstatus")]
        public string AuthUnauthstatus { get; set; }

    }

}
