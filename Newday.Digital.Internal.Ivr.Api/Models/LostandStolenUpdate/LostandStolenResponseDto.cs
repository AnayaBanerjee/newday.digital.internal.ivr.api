﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.LostandStolenUpdate
{
    public class LostandStolenResponseDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("memoSuccess")]
        public bool MemoSuccess { get; set; }
    }
}
