﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.LostandStolenUpdate
{
    public class LostandStolenRequestDto
    {
        //
        // Summary:
        //     Max length = 19, AcctNbr
        // [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("accNbr")]
        public string AccNbr { get; set; }


        //
        // Summary:
        //     Max length = 19, CardNbr
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("cardNbr")]
        public string CardNbr { get; set; }
        //
        // Summary:
        //     Default “L”. Hard Coded
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("cardBlk")]
        public string CardBlk { get; set; }

        //
        // Summary:
        //     0 = Lost 
        //     1 = Stolen
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("lossType")]
        public string LossType { get; set; }

        //
        // Summary:
        //     Today’s date in YYYYMMDD
        [JsonProperty("lossDate")]
        [StringLength(8, ErrorMessage = "value should be within range")]
        public string LossDate { get; set; }

        //
        // Summary:
        //     Default “GBR”. Hardcoded
        [StringLength(3, ErrorMessage = "value should be within range")]
        [JsonProperty("countryOfLoss")]
        public string CountryOfLoss { get; set; }

        //
        // Summary:
        //     Space (default)
        [StringLength(2, ErrorMessage = "value should be within range")]
        [JsonProperty("lossLocation")]
        public string LossLocation { get; set; }

        //
        // Summary:
        //     0 = Yes
        //     1 = No
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("newCard")]
        public string NewCard { get; set; }

        //
        // Summary:
        //     0 = No (default)
        //     1 = Yes
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("pinCompromised")]
        public string PinCompromised { get; set; }


        [StringLength(2, ErrorMessage = "value should be within range")]
        [JsonProperty("processType")]
        public string ProcessType { get; set; }
    }
}
