﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.Atc
{
    public class AccountTreatmentChanges
    {
        [JsonProperty("atcId")]
        public string atcId { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("atcType")]
        public string atcType { get; set; }

        [JsonProperty("responseDeadlineDate")]
        public string responseDeadlineDate { get; set; }
    }
}
