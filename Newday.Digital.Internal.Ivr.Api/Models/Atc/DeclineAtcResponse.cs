﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Models.Atc
{
    public class DeclineAtcResponse
    {
        public string type { get; set; }
        public string status { get; set; }
    }
}
