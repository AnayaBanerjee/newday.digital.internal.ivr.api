﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Newday.Digital.Internal.Ivr.Api.Models.Atc
{
    public class UpgradeOfferAtcResponse
    {


        [JsonProperty("accountTreatmentChanges")]
        public List<AccountTreatmentChanges> accountTreatmentChanges { get; set; }


    }
}
