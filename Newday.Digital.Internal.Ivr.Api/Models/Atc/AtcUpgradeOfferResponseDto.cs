﻿
using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.Atc
{
    public class AtcUpgradeOfferResponseDto
    {

        [JsonProperty("atcId")]
        public string AtcId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("atcType")]
        public string AtcType { get; set; }

        [JsonProperty("responseDeadlineDate")]
        public string ResponseDeadlineDate { get; set; }
    }
}
