﻿using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.Atc
{
    public class AtcUpgradeOfferRequestDto
    {       
        [Required(ErrorMessage = "Account is required")]
        [StringLength(19, MinimumLength = 16)]
        [RegularExpression("[0-9]+", ErrorMessage = "Account must be numeric")]
        public string account { get; set; }

        [Required(ErrorMessage = "Customer Number is required")]
        [StringLength(19, MinimumLength = 16)]
        [RegularExpression("[0-9]+", ErrorMessage = "Customer Number must be numeric")]
        public string accountId { get; set; }

        [StringLengthAttribute(10, MinimumLength = 7)]
        [Required]
        public string atcStatus { get; set; }

        [StringLengthAttribute(10, MinimumLength = 7)]
        [Required]
        public string atcType { get; set; }
    }
}
