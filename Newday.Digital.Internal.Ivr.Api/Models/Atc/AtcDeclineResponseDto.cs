﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.Atc
{
    public class AtcDeclineResponseDto
    {

        [JsonProperty("success")]
        public string Success { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("accountId")]
        public string AccountId { get; set; }
        [JsonProperty("account")]
        public string Account { get; set; }
    }
}
