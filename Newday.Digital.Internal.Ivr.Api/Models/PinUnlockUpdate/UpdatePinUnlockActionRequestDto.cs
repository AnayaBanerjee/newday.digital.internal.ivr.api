﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.PinUnlockUpdate
{
    public class UpdatePinUnlockActionRequestDto
    {
        //
        // Summary:
        //     Max length = 19, ACCNT NBR
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("accntNbr")]
        public string AccntNbr { get; set; }
        //
        // Summary:
        //     Max length = 19, CARD NBR
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("cardNbr")]
        public string CardNbr { get; set; }
        //
        // Summary:
        //     Max length = 1, FOREIGN USE
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("foreignUse")]
        public string ForeignUse { get; set; }

        //
        // Summary:
        //     Max length = 1, INVPIN Tries
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("invPinTries")]
        public string InvPinTries { get; set; }


    }
}
