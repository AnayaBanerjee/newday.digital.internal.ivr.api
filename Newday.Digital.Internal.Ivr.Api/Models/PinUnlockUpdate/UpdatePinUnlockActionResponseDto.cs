﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.PinUnlockUpdate
{
    public class UpdatePinUnlockActionResponseDto
    {

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("memoSuccess")]
        public bool MemoSuccess { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

}
