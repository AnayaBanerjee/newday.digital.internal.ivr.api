﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.MemoUpdate
{
    public class UpdateMemoResponseDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("memoSuccess")]
        public bool MemoSuccess { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
