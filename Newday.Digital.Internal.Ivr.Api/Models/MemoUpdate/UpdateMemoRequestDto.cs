﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.MemoUpdate
{
    public class UpdateMemoRequestDto
    {
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("account")]
        public string Account { get; set; }

        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("crdnbr")]
        public string CrdNbr { get; set; }

        [StringLength(200, ErrorMessage = "value should be within range")]
        [JsonProperty("memoline2")]
        public string MemoLine2 { get; set; }

        [StringLength(11, ErrorMessage = "value should be within range")]
        [JsonProperty("amt")]
        public string Amt { get; set; }

        [StringLength(200, ErrorMessage = "value should be within range")]
        [JsonProperty("memoline3")]
        public string MemoLine3 { get; set; }

        [StringLength(11, ErrorMessage = "value should be within range")]
        [JsonProperty("actncd")]
        public string ActnCd { get; set; }

        [StringLength(200, ErrorMessage = "value should be within range")]
        [JsonProperty("memoline4")]
        public string MemoLine4 { get; set; }

        [StringLength(200, ErrorMessage = "value should be within range")]
        [JsonProperty("memoline5")]
        public string MemoLine5 { get; set; }

        [StringLength(200, ErrorMessage = "value should be within range")]
        [JsonProperty("memoline1")]
        public string MemoLine1 { get; set; }
    }
}

