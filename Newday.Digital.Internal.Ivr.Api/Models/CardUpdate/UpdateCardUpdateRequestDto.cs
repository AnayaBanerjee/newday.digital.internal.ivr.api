﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Newday.Digital.Internal.Ivr.Api.Models.CardUpdate
{
    public class UpdateCardUpdateRequestDto
    {
        //
        // Summary:
        //     Max length = 19, ACCNT NBR
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("accntNbr")]
        public string AccntNbr { get; set; }
        //
        // Summary:
        //     Max length = 19, CARD NBR
        [StringLength(19, ErrorMessage = "value should be within range")]
        [JsonProperty("cardNbr")]
        public string CardNbr { get; set; }
        //
        // Summary:
        //     Max length = 1, FOREIGN USE
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("foreignUse")]
        public string ForeignUse { get; set; }

        //
        // Summary:
        //     Max length = 1, INVPIN Tries
        [StringLength(1, ErrorMessage = "value should be within range")]
        [JsonProperty("alRestriction")]
        public string AlRestriction { get; set; }


    }
}
