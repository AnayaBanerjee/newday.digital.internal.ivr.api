﻿using Newtonsoft.Json;

namespace Newday.Digital.Internal.Ivr.Api.Models.Exception
{
    public class ExceptionResponseDto
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
