﻿namespace Newday.Digital.Internal.Ivr.Api.Models
{
    public class AtcServerConfig
    {
        public string BaseUrl { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string Scope { get; set; }

        public string IdentityUrl { get; set; }

        public string Context_Tenantid { get; set; }
    }
}
