﻿using Newday.Digital.Internal.Ivr.Api.Models.Exception;
using NewDay.Digital.Foundation.Connector.FirstData.Exceptions;
using System;
using System.Runtime.CompilerServices;

namespace Newday.Digital.Internal.Ivr.Api
{
    public class MethodHelper
    {
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod([System.Runtime.CompilerServices.CallerMemberName] string CallerMemberName = "")
        {
            return CallerMemberName;
        }

        public static string GetExceptionMessage(Exception ex)
        {
            if (ex is FirstDataApiException e)
            {
                return e.ResponseStatus.ErrorCode + " " + e.ResponseStatus.Message;
            }

            return ex.Message;
        }

        public static ExceptionResponseDto GetExceptionMessageJson(Exception ex)
        {
            ExceptionResponseDto exception = new ExceptionResponseDto();
            if (ex is FirstDataApiException e)
            {

                exception.Success = false;
                exception.Id = e.ResponseStatus.ErrorCode;
                exception.Description = e.ResponseStatus.Message;
                return exception;
            }
            exception.Success = false;
            exception.Id = "E001";
            exception.Description = ex.Message;
            return exception;
        }

        public static ExceptionResponseDto GetExceptionMessageDeflection(string s)
        {
            ExceptionResponseDto exception = new ExceptionResponseDto();

            exception.Success = false;
            exception.Id = "E002";
            exception.Description = s;
            return exception;
        }

        public static ExceptionResponseDto GetExceptionMessagelostJson(Exception ex)
        {
            ExceptionResponseDto exception = new ExceptionResponseDto();
            if (ex is FirstDataApiException e)
            {
                String message = e.ResponseStatus.Message;
                String error = message.Substring(message.IndexOf("VPL"), 10);
                exception.Success = false;
                exception.Id = error;
                exception.Description = e.ResponseStatus.Message;
                return exception;
            }
            exception.Success = false;
            exception.Id = "E001";
            exception.Description = ex.Message;
            return exception;
        }

        public static ExceptionResponseDto GetExceptionMessagevalidationJson(String field)
        {
            ExceptionResponseDto exception = new ExceptionResponseDto();
            exception.Success = false;
            exception.Id = "E002";
            exception.Description = "Invalid value in parameter " + field;
            return exception;
        }
    }
}