﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.MonetaryActionUpdate;
using NewDay.Digital.Foundation.Connector.FirstData.Transactions.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonetaryActionController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly IAccountUpdateService _accountUpdateService;
        private readonly ILogger _logger;

        public MonetaryActionController(IBrandService brandService,
            IAccountUpdateService accountUpdateService,
            ILogger<MonetaryActionController> logger)
        {
            _brandService = brandService;
            _accountUpdateService = accountUpdateService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<UpdateMonetaryActionResponseDto>> UpdateMonetaryAction([FromBody] UpdateMonetaryActionRequestDto updateMonetaryActionRequestDto)
        {
            try
            {
                var common = new NewDay.Digital.Foundation.Connector.FirstData.Models.Common();
                //String org = updateMonetaryActionRequestDto.AcctNbr.Substring(0, 3);
                /*
                if (org.Equals("000"))
                {
                    org = updateMonetaryActionRequestDto.AcctNbr.Substring(3, 3);
                }*/

                String Line1 = "", Line2 = "", Line3 = "";
                if (updateMonetaryActionRequestDto.Line1.Length > 60)
                {
                    Line1 = updateMonetaryActionRequestDto.Line1.Substring(0, 60);
                    if (updateMonetaryActionRequestDto.Line1.Length > 120)
                    {
                        Line2 = updateMonetaryActionRequestDto.Line1.Substring(60, 60);
                        Line3 = updateMonetaryActionRequestDto.Line1.Substring(120, updateMonetaryActionRequestDto.Line1.Length - 120);
                    }
                    else
                    {
                        Line2 = updateMonetaryActionRequestDto.Line1.Substring(60, updateMonetaryActionRequestDto.Line1.Length - 60);
                    }

                }
                else
                {
                    Line1 = updateMonetaryActionRequestDto.Line1;
                }
                var brand = _brandService.GetBrandFromAccountNumber(updateMonetaryActionRequestDto.AcctNbr);
                // common.Org = org;
                common.ClientNumber = brand.ClientNumber;
                var request = new MonetaryActionRequest(brand.ClientNumber)
                {
                    AcctNbr = updateMonetaryActionRequestDto.AcctNbr,
                    TxnAmount = updateMonetaryActionRequestDto.TxnAmount,
                    PlanNbr = "10000",
                    PlanSeq = "1",
                    ForeignUse = updateMonetaryActionRequestDto.ForeignUse,
                    ActionCode = updateMonetaryActionRequestDto.ActionCode,
                    EffDate = updateMonetaryActionRequestDto.EffDate,
                    StoreNbr = updateMonetaryActionRequestDto.StoreNbr,
                    Line1 = Line1,
                    Line2 = Line2,
                    Line3 = Line3

                };

                var result = await _accountUpdateService.MonetaryActionUpdate(request);

                var response = new UpdateMonetaryActionResponseDto()
                {
                    Success = true
                };

                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }

    }
}