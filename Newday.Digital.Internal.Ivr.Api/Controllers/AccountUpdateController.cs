﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.DirectCreditUpdate;
using NewDay.Digital.Foundation.Connector.FirstData.AccountUpdate.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountUpdateController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly IAccountUpdateService _accountUpdateService;
        private readonly ILogger _logger;

        public AccountUpdateController(IBrandService brandService,
            IAccountUpdateService accountUpdateService,
            ILogger<AccountUpdateController> logger)
        {
            _brandService = brandService;
            _accountUpdateService = accountUpdateService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<UpdateDirectCreditResponseDto>> UpdateDirectCredit([FromBody] UpdateDirectCreditRequestDto updateDirectCreditRequestDto)
        {
            try
            {
                var common = new Common();
                String org = updateDirectCreditRequestDto.Account.Substring(0, 3);
                if (org.Equals("000"))
                {
                    org = updateDirectCreditRequestDto.Account.Substring(3, 3);
                }
                int str = (int)Int64.Parse(org);
                //_logger.Log(LogLevel.Trace, "IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    //User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, "value of org" + str);
                var brand = _brandService.GetBrandFromAccountNumber(updateDirectCreditRequestDto.Account);
                common.Org = org;
                common.ClientNumber = brand.ClientNumber;
                var request = new DirectCreditUpdateRequest(brand.ClientNumber)
                {
                    Common = common,
                    DcAchDbNbr = updateDirectCreditRequestDto.Bankacctnum,
                    AcctNbr = updateDirectCreditRequestDto.Account,
                    DcAchPlan = "0",
                    DcAchRtNbr = updateDirectCreditRequestDto.SortCode,
                    DcType = "1",
                    DcAchDbType = "S",
                    DcAchReqDay = "0",
                    //  OnDemandDcIban = "0",
                    // DcAchReqDayInd = "0",
                    DcAchAmt = "0",
                    DcAchStartDate = "00000000",
                    DcAchExpireDate = "00000000"
                };


                var result = await _accountUpdateService.DirectCreditUpdate(request);

                var response = new UpdateDirectCreditResponseDto()
                {
                    Success = true
                };
                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }

    }
}