﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.MemoUpdate;
using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MemoUpdateController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly IAccountUpdateService _accountUpdateService;
        private readonly ILogger _logger;


        public MemoUpdateController(IBrandService brandService,
            IAccountUpdateService accountUpdateService,
            ILogger<MemoUpdateController> logger)
        {
            _brandService = brandService;
            _accountUpdateService = accountUpdateService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<UpdateMemoResponseDto>> UpdateMemo([FromBody] UpdateMemoRequestDto updateMemoRequestDto)
        {
            try
            {
                //acct no validation 
                if (updateMemoRequestDto.Account.Length > 19 || updateMemoRequestDto.Account.Equals(""))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("acctNbr"));
                }
                //action code
                if (updateMemoRequestDto.ActnCd.Length != 0)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("ActnCd"));
                }
                //memo line validation
                if (updateMemoRequestDto.MemoLine1.Length != 0)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("MemoLine1"));
                }

                int str = (int)Int64.Parse(updateMemoRequestDto.Account);
                _logger.LogTrace("Log Account {updateMemoRequestDto.account}", str);
                var brand = _brandService.GetBrandFromAccountNumber(updateMemoRequestDto.Account);
                _logger.LogTrace("Log Brand details {brand}", brand);

                var request = new MemoAddRequest(brand.ClientNumber)
                {
                    Account = updateMemoRequestDto.Account,
                    ActnCd = updateMemoRequestDto.ActnCd,
                    MemoLine1 = updateMemoRequestDto.MemoLine1
                };
                var result = await _accountUpdateService.MemoUpdate(request);

                _logger.LogTrace("Log result {result}", result.ResponseStatus);

                var response = new UpdateMemoResponseDto()
                {
                    Success = true
                };
                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }
    }
}