﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.File;
using Microsoft.Extensions.Configuration;
using Newday.Digital.Internal.Ivr.Api.Models.DeflectionStatus;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefectStatusController : ControllerBase
    {
        private readonly IConfiguration Configuration;
        public DefectStatusController(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        [HttpPost]
        public ActionResult<DeflectStatusResponseDto> DeflectStatus([FromBody] DeflectStatusRequestDto deflectStatusRequestDto)
        {
            try
            {
                string result = "";
                string brand = "";
                string skill = "";
                string deflectstatus = "";
                string brandstatus = "";
                string skillstatus = "";
                string authunauthstatus = "";
                string authunauth = "";
                //code for retrieving the connection string  
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Configuration["ConnectionString"]);

                CloudFileClient cloudFileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare fileShare = cloudFileClient.GetShareReference(Configuration["Storage"]);

                // Ensure that the share exists.  
                if (fileShare.Exists())
                {
                    // Get a reference to the root directory for the share.  
                    CloudFileDirectory fileDirectory = fileShare.GetRootDirectoryReference();

                    // Get a reference to the directory we created previously.  
                    CloudFileDirectory customDirectory = fileDirectory.GetDirectoryReference("DeflectStatus");

                    if (deflectStatusRequestDto.Dept.Equals("CL"))
                    {
                        if (deflectStatusRequestDto.AuthUnauthstatus.Equals("Authenticated"))
                        {
                            customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusCL").GetDirectoryReference("Auth");
                        }
                        else
                        {
                            customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusCL").GetDirectoryReference("UnAuth");
                        }
                    }
                    else if (deflectStatusRequestDto.Dept.Equals("Disputed"))
                    {
                        customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusDisputed");
                    }
                    else if (deflectStatusRequestDto.Skill.Equals("IVR Own Brand"))
                    {
                        if (deflectStatusRequestDto.AuthUnauthstatus.Equals("Authenticated"))
                        {
                            customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusOwnBrand").GetDirectoryReference("Auth");
                        }
                        else
                        {
                            customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusOwnBrand").GetDirectoryReference("UnAuth");
                        }
                    }
                    else if (deflectStatusRequestDto.Brand.Equals("Debenhams"))
                    {
                        customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusDebenhams");
                    }
                    else if (deflectStatusRequestDto.Brand.Equals("Amazon"))
                    {
                        customDirectory = fileDirectory.GetDirectoryReference("DeflectStatusAmazon");
                    }
                    else if (deflectStatusRequestDto.Skill.Equals("IVR Co Brand"))
                    {
                        if (deflectStatusRequestDto.AuthUnauthstatus.Equals("Authenticated"))
                        {
                            customDirectory = fileDirectory.GetDirectoryReference("DeflectStatus").GetDirectoryReference("Auth");
                        }
                        else
                        {
                            customDirectory = fileDirectory.GetDirectoryReference("DeflectStatus").GetDirectoryReference("UnAuth");
                        }

                    }

                    // Ensure that the directory exists.  
                    if (customDirectory.Exists())
                    {
                        string filename = "";
                        IEnumerable<IListFileItem> fileList = customDirectory.ListFilesAndDirectories();
                        foreach (IListFileItem listItem in fileList)
                        {

                            if (listItem.GetType() == typeof(CloudFile))
                            {
                                if (filename.Length == 0)
                                {
                                    filename = listItem.Uri.Segments.Last().ToString();
                                }
                                else
                                {
                                    //code where the latest version is being checked and picked for getting the contents
                                    string name = listItem.Uri.Segments.Last();
                                    int version = Int32.Parse(name.Substring(3, name.Length - 7));
                                    int version2 = Int32.Parse(filename.Substring(3, filename.Length - 7));
                                    if (Int32.Parse(name.Substring(3, name.Length - 7)) > Int32.Parse(filename.Substring(3, filename.Length - 7)))
                                    {
                                        filename = name;
                                    }
                                    //if(name.Substring(3,name.Length-3))
                                }

                            }
                        }
                        // Get a reference to the file we picked up in our last step  
                        CloudFile fileInfo = customDirectory.GetFileReference(filename);

                        // Ensure that the file exists.  
                        if (fileInfo.Exists())
                        {
                            result = fileInfo.DownloadTextAsync().Result;
                        }
                        string[] arr = result.Split(",");
                        foreach (string ListItem in arr)
                        {
                            string[] status = ListItem.ToString().Split(":");
                            if (status.GetValue(0).ToString().ToUpper().Equals(deflectStatusRequestDto.Skill.ToUpper()))
                            {
                                skillstatus = (string)status.GetValue(1);
                                skill = status.GetValue(0).ToString().ToUpper();
                            }
                            else if (status.GetValue(0).ToString().ToUpper().Equals(deflectStatusRequestDto.Brand.ToUpper()))
                            {
                                brandstatus = (string)status.GetValue(1);
                                brand = status.GetValue(0).ToString().ToUpper();
                            }
                            else if (status.GetValue(0).ToString().ToUpper().Equals(deflectStatusRequestDto.AuthUnauthstatus.ToUpper()))
                            {
                                authunauthstatus = (string)status.GetValue(1);
                                authunauth = status.GetValue(0).ToString().ToUpper();

                            }

                        }
                    }

                }
                if (brandstatus.Equals("false"))
                {
                    deflectstatus = brandstatus;
                }
                else if (authunauthstatus.Equals("false"))
                {

                    deflectstatus = authunauthstatus;
                }
                else
                {
                    deflectstatus = skillstatus;
                }
                if ((!brand.Equals("") && brand != null) || (!skill.Equals("") && skill != null))
                {
                    var response = new DeflectStatusResponseDto()
                    {
                        Success = true,
                        Deflect_status = deflectstatus,
                        Brand = brand,
                        Skill = skill,
                        Auth_unauth = authunauth
                    };
                    return response;

                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessageDeflection("The Brand or Skill doesn't exist in config"));
                }

            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }
    }
}