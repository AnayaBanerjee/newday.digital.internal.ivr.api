﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.LostandStolenUpdate;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v2.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LostandStolenController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly ILostandStolenUpdateService _loststolenservice;
        private readonly ILogger _logger;

        public LostandStolenController(IBrandService brandService, ILostandStolenUpdateService lostandstolenservice,
            ILogger<LostandStolenController> logger)
        {
            _brandService = brandService;
            _loststolenservice = lostandstolenservice;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<LostandStolenResponseDto>> UpdateLostAndStolen([FromBody] LostandStolenRequestDto lostandStolenRequestDto)
        {
            try
            {
                //acct no validation 
                if (lostandStolenRequestDto.AccNbr.Length > 19 || lostandStolenRequestDto.AccNbr.Equals(""))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("acctNbr"));
                }
                //card no validation
                if (lostandStolenRequestDto.CardNbr.Length > 19 || lostandStolenRequestDto.CardNbr.Equals(""))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("cardNbr"));
                }
                //card block code validation
                if (lostandStolenRequestDto.CardBlk.Length > 1 || lostandStolenRequestDto.CardBlk.Equals("") || Char.IsDigit(lostandStolenRequestDto.CardBlk.ElementAt(0)) || lostandStolenRequestDto.CardBlk.Equals(" "))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("cardBlk"));
                }
                //process type validation
                if (!lostandStolenRequestDto.ProcessType.Equals("0"))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("ProcessType - Must be 0"));
                }
                // new card validation
                if (!lostandStolenRequestDto.NewCard.Equals("0") && !lostandStolenRequestDto.NewCard.Equals("1"))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("NewCard - Must be 0 or 1"));
                }
                //pin compromised validation
                if (!lostandStolenRequestDto.PinCompromised.Equals("0") && !lostandStolenRequestDto.PinCompromised.Equals("1"))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("PinCompromised - valid values are 0 & 1"));
                }
                //country of loss validation
                if (!lostandStolenRequestDto.CountryOfLoss.Equals("GBR"))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("CountryOfLoss - Value should be GBR"));
                }

                // loss date validation
                DateTime dateTime = new DateTime();
                if (lostandStolenRequestDto.LossDate.Length == 8)
                {
                    String date = lostandStolenRequestDto.LossDate.Substring(0, 4) + "-" + lostandStolenRequestDto.LossDate.Substring(4, 2) + "-" + lostandStolenRequestDto.LossDate.Substring(6, 2);
                    try
                    {
                        //dateTime = DateTime.TryParseExact(lostandStolenRequestDto.LossDate, "YYYYMMDD", System.Globalization.CultureInfo.InvariantCulture);
                        dateTime = DateTime.Parse(date);
                    }
                    catch (Exception ex)
                    {
                        _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(ex), ex.InnerException);

                        return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("LossDate - Must be in date format YYYYMMDD"));
                    }
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("LossDate - Must be in date format YYYYMMDD"));
                }
                //loss location validation
                if (!lostandStolenRequestDto.LossLocation.Equals(" "))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("LossLocation - Must be a single space"));
                }

                // loss type validation
                if (!lostandStolenRequestDto.LossType.Equals("0") && !lostandStolenRequestDto.LossType.Equals("1"))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, MethodHelper.GetExceptionMessagevalidationJson("LossType - Must be 0 or 1"));
                }
                var brand = _brandService.GetBrandFromAccountNumber(lostandStolenRequestDto.AccNbr);
                var request = new LostStolenUpdateRequest2(brand.ClientNumber)
                {
                    CardBlk = lostandStolenRequestDto.CardBlk,
                    NewCard = lostandStolenRequestDto.NewCard,
                    ProcessType = lostandStolenRequestDto.ProcessType,
                    LossType = lostandStolenRequestDto.LossType,
                    LossLocation = lostandStolenRequestDto.LossLocation,
                    CardNbr = lostandStolenRequestDto.CardNbr,
                    CountryOfLoss = lostandStolenRequestDto.CountryOfLoss,
                    LossDate = lostandStolenRequestDto.LossDate,
                    PinCompromised = lostandStolenRequestDto.PinCompromised,
                    PoliceRefNbr = "0"
                };


                var result = await _loststolenservice.LostAndStolen(request);

                var response = new LostandStolenResponseDto()
                {
                    Success = true,
                    Message = "Lost and stolen request success"

                };
                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessagelostJson(e));
            }
        }

    }
}