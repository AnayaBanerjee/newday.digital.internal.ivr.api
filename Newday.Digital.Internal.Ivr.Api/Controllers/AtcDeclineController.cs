﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.Atc;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AtcDeclineController : ControllerBase
    {
        private readonly IAtcService _atcService;
        private readonly ILogger _logger;
        public AtcDeclineController(IAtcService atcService,
            ILogger<AtcDeclineController> logger)
        {
            _atcService = atcService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<AtcDeclineResponseDto>> AtcDecline([FromBody] AtcDeclineRequestDto atcDeclineRequestDto)
        {
            try
            {
                var apiResponse = new DeclineAtcResponse();
                apiResponse = await _atcService.DeclineOffer(atcDeclineRequestDto);

                _logger.Log(LogLevel.Information, "IVR API ATC Decline Response - Account = {account} , Status = {Status}, Type = {Type}", atcDeclineRequestDto.account,
                    apiResponse.status, apiResponse.type);
                if (apiResponse.status.Equals("Declined"))
                {
                    return new AtcDeclineResponseDto()
                    {
                        Success = "Success",
                        Status = apiResponse.status,
                        Account = atcDeclineRequestDto.account,
                        AccountId = atcDeclineRequestDto.accountId
                    };
                }
                else if (apiResponse.status.Equals("422"))
                {
                    return new AtcDeclineResponseDto()
                    {
                        Success = "Failure",
                        Status = apiResponse.type,
                        Account = atcDeclineRequestDto.account,
                        AccountId = atcDeclineRequestDto.accountId
                    };
                }
                else return new AtcDeclineResponseDto()
                {
                    Success = "Failure",
                    Status = "Error in processing request",
                    Account = atcDeclineRequestDto.account,
                    AccountId = atcDeclineRequestDto.accountId
                };
            }

            catch (Exception e)
            {
                _logger.LogError(e, "ERROR IVR API - User={User} - MethodName={MethodName} - ErrorMessage={errorMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), MethodHelper.GetExceptionMessage(e));

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));

            }
        }
    }
}






