﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.Atc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ATCUpgradeOfferController : ControllerBase
    {
        private readonly IAtcService _atcService;
        private readonly ILogger _logger;
        public ATCUpgradeOfferController(IAtcService atcService,
            ILogger<ATCUpgradeOfferController> logger)
        {
            _atcService = atcService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<AtcUpgradeOfferResponseDto>> AtcUpgrade([FromBody] AtcUpgradeOfferRequestDto atcUpgradeOfferRequestDto)
        {
            try
            {

                var apiResponse = new UpgradeOfferAtcResponse();
                apiResponse = await _atcService.UpgradeOffer(atcUpgradeOfferRequestDto);
                List<AccountTreatmentChanges> accountTreatment = new List<AccountTreatmentChanges>();
                accountTreatment = apiResponse.accountTreatmentChanges;
                var response = new AtcUpgradeOfferResponseDto();
                if (accountTreatment != null)
                {

                    foreach (var accTremnt in accountTreatment)
                    {

                        response.AtcId = accTremnt.atcId;
                        response.AtcType = accTremnt.atcType;
                        response.ResponseDeadlineDate = accTremnt.responseDeadlineDate;
                        response.Status = accTremnt.status;

                    }

                }
                else
                {
                    response.AtcId = "";
                    response.AtcType = "";
                    response.ResponseDeadlineDate = "";
                    response.Status = "";
                }


                return response;
            }

            catch (Exception e)
            {
                _logger.LogError(e, "ERROR IVR API - User={User} - MethodName={MethodName} - ErrorMessage={errorMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), MethodHelper.GetExceptionMessage(e));

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));

            }

        }
    }
}
