﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.PinUnlockUpdate;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PinUnlockUpdateController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly IPinUnlockUpdateService _pinUnlockUpdateService;
        private readonly ILogger _logger;

        public PinUnlockUpdateController(IBrandService brandService,
            IPinUnlockUpdateService pinUnlockUpdateService,
            ILogger<PinUnlockUpdateController> logger)
        {
            _brandService = brandService;
            _pinUnlockUpdateService = pinUnlockUpdateService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<UpdatePinUnlockActionResponseDto>> UpdatePinUnlock([FromBody] UpdatePinUnlockActionRequestDto updatePinUnlockRequestDto)
        {
            try
            {
                var brand = _brandService.GetBrandFromAccountNumber(updatePinUnlockRequestDto.AccntNbr);
                var request = new CardUpdateRequest(brand.ClientNumber)
                {
                    CardNbr = updatePinUnlockRequestDto.CardNbr,
                    ForeignUse = updatePinUnlockRequestDto.ForeignUse,
                    InvPinTries = updatePinUnlockRequestDto.InvPinTries,
                };


                var result = await _pinUnlockUpdateService.PinUnlockUpdate(request);

                var response = new UpdatePinUnlockActionResponseDto()
                {
                    Success = true
                };
                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }

    }
}