﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.CardEnquiry;
using NewDay.Digital.Foundation.Connector.FirstData.CardManagement.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardEnquiryController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly ICardEnquiryService _cardenquiryservice;
        private readonly ILogger _logger;

        public CardEnquiryController(IBrandService brandService, ICardEnquiryService cardenquiryservice,
            ILogger<CardEnquiryController> logger)
        {
            _brandService = brandService;
            _cardenquiryservice = cardenquiryservice;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<CardEnquiryResponseDto>> CardInquiry([FromBody] CardEnquiryRequestDto cardEnquiryRequestDto)
        {
            try
            {
                var common = new NewDay.Digital.Foundation.Connector.FirstData.Models.Common();
                var brand = _brandService.GetBrandFromAccountNumber(cardEnquiryRequestDto.AccntNbr);
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - clientnumber =" + brand.ClientNumber);
                common.ClientNumber = brand.ClientNumber;

                var request = new CardInquiryRequest(brand.ClientNumber)
                {
                    Common = common,
                    Account = cardEnquiryRequestDto.Cardnbr

                };


                var result = await _cardenquiryservice.CardInquiry(request);
                var response = new CardEnquiryResponseDto()
                {
                    CardNumber = result.CardNbr,
                    FraudMonitorFlag = result.FraudMonitorFlag,
                    Success = true,
                    EmbPinCommPref = result.EmbPinCommPref,
                    FreezeStatus = result.RestrAll,
                    Message = "Card Enquiry Successful"

                };
                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }

    }
}