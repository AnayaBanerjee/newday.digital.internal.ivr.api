﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models.CardUpdate;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CardUpdateController : ControllerBase
    {
        private readonly IBrandService _brandService;
        private readonly ICardUpdateService _cardUpdateService;
        private readonly ILogger _logger;

        public CardUpdateController(IBrandService brandService,
            ICardUpdateService cardUpdateService,
            ILogger<CardUpdateController> logger)
        {
            _brandService = brandService;
            _cardUpdateService = cardUpdateService;
            _logger = logger;
        }
        [HttpPost]
        public async Task<ActionResult<UpdateCardUpdateResponseDto>> UpdateCardUpdate([FromBody] UpdateCardUpdateRequestDto cardUpdateRequestDto)
        {
            try
            {
                var brand = _brandService.GetBrandFromAccountNumber(cardUpdateRequestDto.AccntNbr);
                var request = new CardUpdateRequest(brand.ClientNumber)
                {
                    CardNbr = cardUpdateRequestDto.CardNbr,
                    ForeignUse = cardUpdateRequestDto.ForeignUse,
                    AlRestriction = cardUpdateRequestDto.AlRestriction
                };


                var result = await _cardUpdateService.CardUpdate(request);
                String message;
                if (cardUpdateRequestDto.AlRestriction.Equals("0"))
                {
                    message = "Card Unfreeze Successful";
                }
                else
                {
                    message = "Card Freeze Successful";
                }

                var response = new UpdateCardUpdateResponseDto()
                {
                    Success = true,
                    Message = message
                };
                return response;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - User={User} - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                User.Identity.Name, MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);

                return StatusCode(StatusCodes.Status500InternalServerError, MethodHelper.GetExceptionMessageJson(e));
            }
        }

    }
}
