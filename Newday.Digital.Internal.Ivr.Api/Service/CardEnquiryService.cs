﻿using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using NewDay.Digital.Foundation.Connector.FirstData.CardManagement.v1;
using NewDay.Digital.Foundation.Connector.FirstData.CardManagement.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class CardEnquiryService : ICardEnquiryService
    {
        private readonly ICardManagementApiClient _cardenquiryApiClient;
        private readonly ILogger _logger;

        public CardEnquiryService(ICardManagementApiClient cardenquiryApiClient,
             ILogger<AccountUpdateService> logger)
        {
            _cardenquiryApiClient = cardenquiryApiClient;
            _logger = logger;
        }

        public async Task<CardInquiryResponse> CardInquiry(CardInquiryRequest cardEnquiryRequestDto)
        {
            try
            {
                return await _cardenquiryApiClient.CardInquiryAsync(cardEnquiryRequestDto);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }
    }
}