﻿using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1;
using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v2;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v2.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class LostStolenUpdateService : ILostandStolenUpdateService
    {
        private readonly ICardMaintenanceApiClient _loststolenUpdateApiClient;
        private readonly IAccountActivityUpdatesApiClient _accountActivityUpdatesApiClient;
        private readonly ILogger _logger;

        public LostStolenUpdateService(ICardMaintenanceApiClient loststolenUpdateApiClient,
            IAccountActivityUpdatesApiClient accountActivityUpdatesApiClient,
             ILogger<AccountUpdateService> logger)
        {
            _loststolenUpdateApiClient = loststolenUpdateApiClient;
            _accountActivityUpdatesApiClient = accountActivityUpdatesApiClient;
            _logger = logger;
        }

        public async Task<LostStolenUpdateResponse2> LostAndStolen(LostStolenUpdateRequest2 updateLostStolenDto)
        {
            try
            {
                return await _loststolenUpdateApiClient.LostStolenUpdateAsync(updateLostStolenDto);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }

        public async Task<MemoAddResponse> MemoUpdate(MemoAddRequest memoAddRequest)
        {
            try
            {
                return await _accountActivityUpdatesApiClient.MemoAddAsync(memoAddRequest);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }

    }
}