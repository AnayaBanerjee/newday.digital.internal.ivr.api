﻿using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1;
using NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class CardUpdateService : ICardUpdateService
    {
        private readonly ICardMaintenanceApiClient _cardMaintenanceApiClient;
        private readonly ILogger _logger;

        public CardUpdateService(ICardMaintenanceApiClient cardMaintenanceApiClient,
            ILogger<AccountUpdateService> logger)
        {
            _cardMaintenanceApiClient = cardMaintenanceApiClient;
            _logger = logger;
        }

        public async Task<CardUpdateResponse> CardUpdate(CardUpdateRequest request)
        {
            try
            {
                return await _cardMaintenanceApiClient.CardUpdateAsync(request);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR CTI API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }
    }
}