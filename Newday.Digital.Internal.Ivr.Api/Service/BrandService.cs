﻿using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using NewDay.Digital.Foundation.Connector.FirstData.Brands;
using System;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class BrandService : IBrandService
    {
        private readonly IBrandHelper _iBrandHelper;
        private readonly ILogger _logger;

        public BrandService(IBrandHelper iBrandHelper, ILogger<BrandService> logger)
        {
            _iBrandHelper = iBrandHelper;
            _logger = logger;
        }

        public Brand GetBrandFromAccountNumber(string account)
        {
            try
            {
                int str = (int)Int64.Parse(account);
                _logger.LogTrace("Log Account in brand service{account}", str);

                var result = _iBrandHelper.GetBrandFromAccountNumber(account);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception($"Brand service failure: {e.Message}", e);
            }
        }
    }
}

