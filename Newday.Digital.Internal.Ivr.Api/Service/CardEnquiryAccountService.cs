﻿using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using NewDay.Digital.Foundation.Connector.FirstData.AccountManagement.v2;
using NewDay.Digital.Foundation.Connector.FirstData.AccountManagement.v2.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class CardEnquiryAccountService : ICardEnquiryAccountService
    {
        private readonly IAccountManagementApiClient _cardenquiryaccountApiClient;
        private readonly ILogger _logger;

        public CardEnquiryAccountService(IAccountManagementApiClient cardenquiryaccountApiClient,
             ILogger<AccountUpdateService> logger)
        {
            _cardenquiryaccountApiClient = cardenquiryaccountApiClient;
            _logger = logger;
        }

        public async Task<CardsListByAccountResponse2> cardenquiryaccount(CardsListByAccountRequest2 request)
        {
            try
            {
                return await _cardenquiryaccountApiClient.CardsListByAccountAsync(request);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }

    }
}