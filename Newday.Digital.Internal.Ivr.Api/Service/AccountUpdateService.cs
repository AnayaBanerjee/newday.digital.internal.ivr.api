﻿using Microsoft.Extensions.Logging;
using Newday.Digital.Internal.Ivr.Api.Interface;
using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1;
using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.AccountUpdate.v1;
using NewDay.Digital.Foundation.Connector.FirstData.AccountUpdate.v1.Models;
using NewDay.Digital.Foundation.Connector.FirstData.Transactions.v1;
using NewDay.Digital.Foundation.Connector.FirstData.Transactions.v1.Models;
using System;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class AccountUpdateService : IAccountUpdateService
    {
        private readonly IAccountUpdateApiClient _accountUpdateApiClient;
        private readonly IAccountActivityUpdatesApiClient _accountActivityUpdatesApiClient;
        private readonly ITransactionsApiClient _transactionsApiClient;
        private readonly ILogger _logger;

        public AccountUpdateService(IAccountUpdateApiClient accountUpdateApiClient,
            IAccountActivityUpdatesApiClient accountActivityUpdatesApiClient,
            ITransactionsApiClient transactionsApiClient,
             ILogger<AccountUpdateService> logger)
        {
            _accountUpdateApiClient = accountUpdateApiClient;
            _accountActivityUpdatesApiClient = accountActivityUpdatesApiClient;
            _transactionsApiClient = transactionsApiClient;
            _logger = logger;
        }

        public async Task<DirectCreditUpdateResponse> DirectCreditUpdate(DirectCreditUpdateRequest updateDirectCreditDto)
        {
            try
            {
                return await _accountUpdateApiClient.DirectCreditUpdateAsync(updateDirectCreditDto);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR IVR API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }

        public async Task<MemoAddResponse> MemoUpdate(MemoAddRequest memoAddRequest)
        {
            try
            {
                return await _accountActivityUpdatesApiClient.MemoAddAsync(memoAddRequest);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR CTI API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }
        }

        public async Task<MonetaryActionResponse> MonetaryActionUpdate(MonetaryActionRequest request)
        {
            try
            {
                return await _transactionsApiClient.MonetaryActionAsync(request);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, "ERROR CTI API - MethodName={MethodName} - TimeUTC={datetime} - ErrorMessage={errorMessage} - InnerException={innerMessage}",
                    MethodHelper.GetCurrentMethod(), DateTime.UtcNow, MethodHelper.GetExceptionMessage(e), e.InnerException);
                throw;
            }

        }
    }
}