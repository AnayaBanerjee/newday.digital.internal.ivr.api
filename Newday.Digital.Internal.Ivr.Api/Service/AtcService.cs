﻿using IdentityModel.Client;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models;
using Newday.Digital.Internal.Ivr.Api.Models.Atc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Newday.Digital.Internal.Ivr.Api.Service
{
    public class AtcService : IAtcService
    {
        private readonly ILogger _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly AtcServerConfig _atcServerConfig;
        private readonly IConfiguration _configuration;

        public AtcService(ILogger<AtcService> logger, IHttpClientFactory httpClientFactory,
            IOptions<AtcServerConfig> atcServerConfig, IConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _atcServerConfig = atcServerConfig.Value;
            _configuration = configuration;
        }

        /*private async Task<SecretBundle> Cert()
        {
            AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();
            KeyVaultClient keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));
            var certificateSecret = await keyVaultClient.GetSecretAsync(_configuration["KeyVault:certIdentifier"], "voiceapp-atc-cert");
            return certificateSecret;
        }*/

        public async Task<DeclineAtcResponse> DeclineOffer(AtcDeclineRequestDto atcDeclineRequestDto)
        {
            var token = await GetAtcBearerToken();
            var httpClient = _httpClientFactory.CreateClient(HttpClients.AtcClient);
            var request = "Upgrade";
            var jsonRequest = JsonConvert.SerializeObject(request);
            var data = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            using (httpClient)
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("context-tenantid", _atcServerConfig.Context_Tenantid);
                var response = await httpClient.PostAsync(String.Format(_atcServerConfig.BaseUrl + "/account-treatment/account/{0}/atc/{1}/decline/", atcDeclineRequestDto.accountId, atcDeclineRequestDto.atcId), data);
                string content = await response.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Information, "IVR API ATC Decline Service Response - Account = {account} , Response Body = {jsonObject}", atcDeclineRequestDto.account, content);
                if (response.StatusCode.Equals("500"))
                {
                    throw new Exception($"AtcDecline Error {content}");
                }
                var jsonObject = JsonConvert.DeserializeObject<DeclineAtcResponse>(content);
                return jsonObject;
            }
        }
        private async Task<string> GetAtcBearerToken()
        {
            var token = await GetAtcToken();

            if (token.IsError)
            {
                return null;
            }

            return token.AccessToken;
        }

        /*Certificate based authentication*/
        /*private async Task<TokenResponse> GetAtcToken()
        {
            SecretBundle certificateSecret = await Cert();
            var privateKeyBytes = Convert.FromBase64String(certificateSecret.Value);
            var certificate = new X509Certificate2(privateKeyBytes, string.Empty);
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            handler.ClientCertificates.Add(certificate);
            var httpClient = new HttpClient(handler);
            using (httpClient)
            {
                using (httpClient)
                {
                    var formContent = new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("client_id", _atcServerConfig.ClientId),
                        new KeyValuePair<string, string>("grant_type", "client_credentials"),
                        new KeyValuePair<string, string>("scope", _atcServerConfig.Scope),                 

                    };
                    var requestContent = new FormUrlEncodedContent(formContent);
                    httpClient.DefaultRequestHeaders.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = await httpClient.PostAsync(string.Format(_atcServerConfig.IdentityUrl), requestContent);
                    var content = await response.Content.ReadAsStringAsync();
                    var token = JsonConvert.DeserializeObject<TokenResponse>(content);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(string.Format("GetAtcToken Error {0} ", response.StatusCode));
                    }
                    return token;
                }
            }
        }*/

        private async Task<TokenResponse> GetAtcToken()
        {
            var httpClient = _httpClientFactory.CreateClient(HttpClients.AtcClient);
                using (httpClient)
                {
                httpClient.BaseAddress = new Uri(_atcServerConfig.IdentityUrl);
                
                var getToken = await httpClient.GetDiscoveryDocumentAsync();
                
                TokenResponse tokenResponse = await httpClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                    Address = getToken.TokenEndpoint,
                    ClientId = _atcServerConfig.ClientId,
                    ClientSecret = _atcServerConfig.ClientSecret,
                    Scope = _atcServerConfig.Scope
                });
                if (!tokenResponse.HttpResponse.IsSuccessStatusCode)
                    {
                        throw new Exception(string.Format("GetAtcToken Error {0} ", tokenResponse.HttpStatusCode));
                    }
                    return tokenResponse;
                }
            
        }

        public async Task<UpgradeOfferAtcResponse> UpgradeOffer(AtcUpgradeOfferRequestDto atcUpgradeOfferRequestDto)
        {
            var token = await GetAtcBearerToken();
            var httpClient = _httpClientFactory.CreateClient(HttpClients.AtcClient);
            using (httpClient)
            {

                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                httpClient.DefaultRequestHeaders.Add("context-tenantid", _atcServerConfig.Context_Tenantid);
                var response = await httpClient.GetAsync(String.Format(_atcServerConfig.BaseUrl + "/account-treatment/account/{0}/atc?atcStatus={1}&atcType={2}", atcUpgradeOfferRequestDto.accountId, atcUpgradeOfferRequestDto.atcStatus, atcUpgradeOfferRequestDto.atcType));
                string content = await response.Content.ReadAsStringAsync();
                var jsonObject = JsonConvert.DeserializeObject<UpgradeOfferAtcResponse>(content);
                _logger.Log(LogLevel.Information, "IVR API ATC Decline Service Response - Account = {account} , Response Body = {jsonObject}", atcUpgradeOfferRequestDto.account, jsonObject);

                if (!response.IsSuccessStatusCode)
                    throw new Exception($"AtcUgrade Error {content}");
                return jsonObject;
            }
        }
    }
}