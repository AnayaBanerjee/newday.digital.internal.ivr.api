﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.ApplicationInsights;

namespace Newday.Digital.Internal.Ivr.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .CaptureStartupErrors(true) // the default
                .UseSetting("detailedErrors", "true")
                .UseStartup<Startup>()
                .ConfigureLogging((hostingContext, logging) =>
                {
                    var appInsightKey = hostingContext.Configuration["ApplicationInsights:InstrumentationKey"];
                    logging.AddFilter<ApplicationInsightsLoggerProvider>(
                       typeof(Program).FullName, LogLevel.Trace);

                    // Capture all log-level entries from Startup
                    logging.AddFilter<ApplicationInsightsLoggerProvider>(
                        typeof(Startup).FullName, LogLevel.Trace);
                    logging.AddFilter<ApplicationInsightsLoggerProvider>
                        ("NewDay.Digital.Internal.Ivr.Api", LogLevel.Trace);

                    logging.AddApplicationInsights(appInsightKey);
                });
    }
}
