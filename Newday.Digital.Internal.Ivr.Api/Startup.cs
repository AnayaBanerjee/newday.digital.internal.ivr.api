﻿using Microsoft.ApplicationInsights.Extensibility.PerfCounterCollector.QuickPulse;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newday.Digital.Internal.Ivr.Api.Configuration;
using Newday.Digital.Internal.Ivr.Api.Interface;
using Newday.Digital.Internal.Ivr.Api.Models;
using Newday.Digital.Internal.Ivr.Api.Service;
using NewDay.Digital.Foundation.Connector.FirstData.AccountActivityUpdates.v1;
using NewDay.Digital.Foundation.Connector.FirstData.AccountUpdate.v1;
using NewDay.Digital.Foundation.Connector.FirstData.Extensions;
using NewDay.Digital.Foundation.Connector.FirstData.Transactions.v1;
using System;

namespace Newday.Digital.Internal.Ivr.Api
{
    public class Startup
    {
        private readonly ILogger _logger;
        public Startup(IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true); ;
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(sharedOptions =>
            {
                sharedOptions.DefaultScheme = AzureADDefaults.AuthenticationScheme;
                sharedOptions.DefaultChallengeScheme = AzureADDefaults.BearerAuthenticationScheme;
            })
                .AddAzureADBearer(options => Configuration.Bind("AzureAd", options))
                .AddAzureAD(options => Configuration.Bind("AzureAd", options))
                .AddCookie();

            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .AddAuthenticationSchemes(AzureADDefaults.AuthenticationScheme, AzureADDefaults.BearerAuthenticationScheme)
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
                options.JsonSerializerOptions.DictionaryKeyPolicy = null;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddCors(options => options.AddPolicy("AllowAnyOrigin", builder => builder.AllowAnyOrigin().AllowAnyHeader()));


            services.AddNewDayFirstDataConnector(apiBaseAddress: Configuration["FirstData:ApiBaseAddress"],
                apiResourceIdentifier: Configuration["FirstData:ApiResourceIdentifier"],
                brandsConnectionString: Configuration["FirstData:ConnectionString:Brands"],
                requestTimeout: TimeSpan.FromSeconds(Convert.ToDouble(Configuration["FirstData:RequestTimeout"])));
            services.AddLogging();

            services.AddApplicationInsightsTelemetry();
            services.ConfigureTelemetryModule<QuickPulseTelemetryModule>((module, o) => module.AuthenticationApiKey = Configuration["ApplicationInsights:InstrumentationKey"]);
            services.AddHttpClients(Configuration);
            services.Configure<AtcServerConfig>(Configuration.GetSection("AtcServer"));


            services.AddSingleton<IBrandService, BrandService>();
            services.AddTransient<IAccountUpdateApiClient, AccountUpdateApiClient>();
            services.AddTransient<IAccountActivityUpdatesApiClient, AccountActivityUpdatesApiClient>();
            services.AddTransient<ITransactionsApiClient, TransactionsApiClient>();
            services.AddTransient<NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v2.ICardMaintenanceApiClient, NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v2.CardMaintenanceApiClient>();
            services.AddTransient<NewDay.Digital.Foundation.Connector.FirstData.CardManagement.v1.ICardManagementApiClient, NewDay.Digital.Foundation.Connector.FirstData.CardManagement.v1.CardManagementApiClient>();
            services.AddScoped<IAccountUpdateService, AccountUpdateService>();
            services.AddScoped<ILostandStolenUpdateService, LostStolenUpdateService>();
            services.AddScoped<ICardEnquiryService, CardEnquiryService>();
            services.AddScoped<ICardUpdateService, CardUpdateService>();
            services.AddTransient<NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1.ICardMaintenanceApiClient, NewDay.Digital.Foundation.Connector.FirstData.CardMaintenance.v1.CardMaintenanceApiClient>();
            services.AddScoped<IPinUnlockUpdateService, PinUnlockUpdateService>();
            services.AddTransient<IAtcService, AtcService>();
            services.AddSwaggerGen();
            //services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo { Title = "IVR API", Version = "v1" }); });
            /*   services.AddSwaggerGen(c =>
               {
                   c.SwaggerDoc("v1", new Info { title = "IVR API", version = "v1" });
                   c.AddSecurityDefinition("Bearer",
                       new ApiKeyScheme
                       {
                           In = "header",
                           Description = "Please enter into field the word 'Bearer' following by space and JWT",
                           Name = "Authorization",
                           Type = "apiKey"
                       });
                   c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                       { "Bearer", Enumerable.Empty<string>() },
                   });
               });*/

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IVR API", Version = "v1" });
                c.AddSecurityDefinition("Bearer",
                    new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Description = "Please enter into field the word 'Bearer' following by space and JWT",
                        Name = "Authorization",
                        Type = SecuritySchemeType.ApiKey
                    });

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use((context, next) =>
            {
                context.Request.Scheme = "https";
                return next();
            });
            app.UseHsts();
            app.UseRouting();
            app.UseDeveloperExceptionPage();
            app.UseCors("AllowAnyOrigin");
            app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "IVR API");
                c.RoutePrefix = "";
            });

            app.UseHttpsRedirection();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
